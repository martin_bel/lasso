source('2_bag_words.R')
d <- dtm

pkg = c('foreign', 'nnet', 'ggplot2', 'caret')
sapply(pkg, require, c=T)

### Multinomial Logistic Regression
# fit <- multinom(clase ~ abierta+abierto+abogado + abr + abril, data = dtm)

predictors <- names(d)[!names(d) %in% "clase"]

set.seed(1)
inTrainingSet <- createDataPartition(
  d$clase,
  p = 0.7,
  list = FALSE
)

train <- d[inTrainingSet, ]
test <- d[-inTrainingSet, ]

ctrl <- trainControl(
  method = "repeatedcv",
  repeats=1,
  classProbs=TRUE
)

Tune <- train(
  x = train[,1:50],
  y = train$clase,
  method = 'multinom',
  verbose = FALSE,
  metric = 'Accuracy',
  #  tuneGrid = grid,
  trControl = ctrl
)

### Predicciones

Pred <- predict(Tune, test[,predictors])
Probs <- predict(Tune, test[,predictors], type = "prob")

confusionMatrix(Pred, test$clase)


